<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/category/{categorySlug}", name="category.index")
     */
    public function index(string $categorySlug):Response
    {

        // récupération d'une catégory par son slug
        $category = $this->categoryRepository->findOneBy(
            ['slug' => $categorySlug]
        );

        /*
            envoi de données à la vue
                utilisation du second parametre de render sous forme associatif
                twig va récupérer les clés du tableau associatif
        */

        /* 
            appel d'une requete personnalisée
                à créer dans les classe de dépôt (Repository)
            methode de récuperation des resultats : 
                getResult
                getArrauResult
        */
        $subcategories = $this->categoryRepository
                ->findSubCategoryByMainCategorySlug($categorySlug)
                ->getResult();

        // dd($subcategories);

        return $this->render('category/index.html.twig', [
            'category' => $category,
            'subCategories' => $subcategories,
            ]);
    }

    /**
     * @Route("/category/{categorySlug}/{subCategorySlug}", name="category.subCategory")
     */
    public function subCategory(string $categorySlug, string $subCategorySlug):Response
    {
        // récupération de la sous-catégorie dans la base
        $subCategory = $this->categoryRepository->findOneBy(
            ['slug' => $subCategorySlug]
        );
        $gifs = $subCategory->getGifs();

        return $this->render('category/subcategory.html.twig', [
            'subCategory' => $subCategory,
            'gifs' => $gifs
            ]);
    }
}
