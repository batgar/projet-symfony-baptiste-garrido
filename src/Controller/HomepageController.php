<?php

/*
    App : espace de noms défini par défault > composer.json
        App -> dossier source
        Controller -> dossier src/Controller
*/
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class HomepageController extends AbstractController
{
    /*
        annotations : 
            utiliser uniquement les doubles guillemets
            commentaire lu par symfony
            @Route : parametres
                schéla de la route : url saisie
                nom de la route
                    nomeclature : nom du controller . nom de la methode


        récupération de la requête HTTP:
            lister les services symfony console debug:container
            injecter un service :
                créer une propriété qui va socker le service
                créer un constructeur avec un paramètre du meme type que le service
                dans le constructeur: le lien entre la propriété et le paramètre
        RequestStack: service qui contient plusieurs requetes et sous requetes
        Request : requete HTTP

    */

    private RequestStack $requestStack;
    private Request $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    /** 
     * @Route("/", name="homepage.index") 
    */ 
    public function index()
    {
        /*
            débogage:
                dd: dump and die
                dump: affichage dans la barre de débogage de symfony

            Request:
                propiété request : $_POST
                propiété query : $_GET
                propiété files : $_FILES
                propiété headers : en-tetes

         */
        // dump($this->request->headers->get("accept-language"));
        // $response = new Response("Yo");
        // return $response;

        /* 
            Vue twig :
                creer un dossier du meme nom que le controler dans le dossier "templates"
                dans le dossier, créer un fichier du meme nom que la methode avec une extension html.twig
        */
        

        return $this->render('homepage/index.html.twig');
    }
}