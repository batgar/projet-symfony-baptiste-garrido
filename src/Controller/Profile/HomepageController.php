<?php

namespace App\Controller\Profile;

use App\Entity\Gif;
use App\Form\GifType;
use App\Repository\GifRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\String\ByteString;
use Twig\Profiler\Node\EnterProfileNode;

class HomepageController extends AbstractController
{
    private GifRepository $gifRepository;
    private RequestStack $requestStack;
    private Request $request;
    private EntityManagerInterface $manager;

    public function __construct(GifRepository $gifRepository, RequestStack $requestStack, EntityManagerInterface $manager)
    {
        $this->gifRepository = $gifRepository;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
        $this->manager = $manager;
    }

    /** 
     * @Route("/profile", name="profile.homepage.index") 
    */ 
    public function index():Response
    {
        $user = $this->getUser();

        // récupération des gifs par l'id de l'utilisateur
        $gifs = $this->gifRepository->getByUserId( $user->getId() )->getResult();
        
        return $this->render('profile/homepage/index.html.twig',[
            'gifs' => $gifs
        ]);
    }

    /** 
     * @Route("/profile/form", name="profile.homepage.form") 
    */ 
    public function form():Response
    {
        /*
            affichage d'un formulaire
                créer une classe de formulaire reliée à un modèle (entité ou classe) : make:form
                dans la clase de formulaire, définir les types de champs
                dans les champs, definir les contraintes de validation

            Il est fortement recommander de supprimer les typages sur les propriété liées aux images
            pour utiliser la classe UploadedFile de Symfony
        */

        // instance nécessaire à l'affichage d'un formulaire
        
        $model = new Gif();
        $type = GifType::class;

        // création du formulaire
        $form = $this->createForm($type,$model);

        // récuperation des données dans la requete HTTP
        $form->handleRequest($this->request);

        // formulaire valide
        if( $form->isSubmitted() && $form->isValid() ){
            // associer l'utilisateur au gif
            $model->setUser( $this->getUser() );

            // gestion de l'image
            $imageName = ByteString::fromRandom(32)->lower();
            // guessExtension : methode de UploadDile qui permet de récupérer l'extension du fichier
            $imageExtension = $model->getSource()->guessExtension();
            // move : methode de UploadDile qui permet de transferer l'image
            $model->getSource()->move('images', "$imageName. $imageExtension" );

            $model->setSlug("$imageName. $imageExtension");
            $model->setSource("$imageName. $imageExtension");

            // récuperation du modele liée au formulaire
            // dd($model);

            /*
                enregistrement en base de données:
                    EntityManagerInterface qui permet les UPDATE, INSERT et DELETE
                    methode persist : equivaut a INSERT, mise en file d'attente
                    flush : exécute les requetes
            */

            $this->manager->persist($model);

            $this->manager->flush();

            

        }

        // createView : permet de transcrire les propriété du modele en champs HTML
        return $this->render('profile/homepage/form.html.twig', [
            'form' => $form->createView()
        ]);
    }

}