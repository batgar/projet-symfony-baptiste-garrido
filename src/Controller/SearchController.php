<?php

namespace App\Controller;

use App\Form\SearchType;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{

    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route("/search", name="search.index", methods = {"POST"} )
     */
    public function index(Request $request)
    {

        $search = $request->request->get('search');
        
        $categories = $this->categoryRepository->getCategoryByString($search)->getResult();

        $parentVue = [];
        $enfantSolo = [];
        $categoriesFinals = [];

        // tri des parents
        foreach ($categories as $category) {
            if ( $category->getParent() == null ) {
                // array_push($parentVue, $category);
                $parentVue[$category->getName()] = $category;
            }
        }

        // tri des enfants sans parents présent pour eviter les doublons
        foreach ($categories as $category) {
            if ( $category->getParent() != null && ! in_array( $category->getParent() , $parentVue) ){
                // $enfantSolo[$category->getName()] = $category;
                array_push($enfantSolo, $category);
            }
        }

        // récupération des enfants pour les categories parents
        // et ajout dans le tableau final sous forme
        // [ 'parent' => ['enfant1' , 'enfant2' , 'enfant3'... ] ]
        foreach ($parentVue as $catParent) {
            $enfantParent = $this->categoryRepository->findSubCategoryByMainCategorySlug($catParent->getSlug())->getResult();
            $categoriesFinals[$catParent->getName()] = $enfantParent;
        }

        // ajout des categories enfants sans parents a la liste finale
        if ( sizeof($enfantSolo)>0 ) {
            $categoriesFinals["Solo"] = $enfantSolo;
        }



        return $this->render('search/index.html.twig', [
            'search' => $search,
            'categories' => $categoriesFinals
        ]);
    }
}
