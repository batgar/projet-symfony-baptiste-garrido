<?php

namespace App\DataFixtures;

abstract class AbstractDataFixtures
{
    const CATEGORY = [

        'animals' => ['mouse', 'cat', 'dogs', 'horse'],
        'actions' => ['breaking up', 'cooking', 'crying']
    ];
}
