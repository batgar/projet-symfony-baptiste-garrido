<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\AsciiSlugger;



class CategoryFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        foreach ( AbstractDataFixtures::CATEGORY as $cat => $sub_cats) {
            /*
                avec Doctrine, pour manipuler les données d'une table
                il faut absulument passer les entités
            */ 
            $slugger = new AsciiSlugger();
            $mainCategory = new Category();
            $mainCategory
                ->setName($cat)
                ->setSlug($slugger->slug($cat));

            // méthode persist de Doctrine : equivaut a un insert into BD
            $manager->persist($mainCategory);
            
            foreach ($sub_cats as $subcat) {
                $subCategory = new Category();
                $subCategory
                    ->setName($subcat)
                    ->setSlug($slugger->slug($subcat))
                    ->setParent($mainCategory)
                    ;
                $manager->persist($subCategory);

                // mise en memoire les entité pour pouvoir y acceder dans d'autres fixtures
                // addReference : 2 parametres
                //      identifiant unique de la référence
                //      entité liée à la référence
                $this->addReference("subcategory-$subcat", $subCategory);

            }
        };

        // $product = new Product();
        // $manager->persist($product);

        // methode de doctrine qui permet d'éxécuter les requetes
        $manager->flush();
    }
}
