<?php

namespace App\DataFixtures;

use App\Entity\Gif;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;


/*
    L'interface DependentFixtureInterface
        nécessite l'implementation de la methode getDependencies
        permet de préciser les dépendances entre fixtures
*/
class GifFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies():array
    {
        return [
            CategoryFixtures::class,
            UserFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $slugger = new AsciiSlugger();
        
        foreach ( AbstractDataFixtures::CATEGORY as $cat => $subCategories ) {
            foreach ( $subCategories as $subCat ) {
                $gif = new Gif();
                $gif
                    ->setSource( $slugger->slug($subCat)->lower() . '.gif' )
                    ->setSlug( $slugger->slug($subCat)->lower() )
                    ->setCategory($this->getReference("subcategory-$subCat"))
                    ->setUser($this->getReference("user"));
                
                $manager->persist($gif);
            }
        }

        $manager->flush();
    }
}
