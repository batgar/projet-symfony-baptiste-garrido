<?php

namespace App\Form;

use App\Entity\Gif;
use App\Entity\User;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Repository\GifRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

class GifType extends AbstractType
{

    private CategoryRepository $category;

    public function __construct( CategoryRepository $category )
    {
        $this->category = $category;    
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*
            methode add : permet de spécifier un champ au formulaire
                parametres : 
                    identifiant du champ utiliser dans la vue
                    type de champ (string par défault, sinon voir doc)
                    option liées aux champs:
                        contrainte de validation

        */
        $builder
            ->add('source', FileType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'L\'image doit être renseigné' 
                        ]),
                    new Image([
                        'mimeTypes' => ['image/gif', 'image/webp'],
                        'mimeTypesMessage' => 'Le format doit être un gif ou un webp'
                    ])
                ],
                'help' => "Seul les gifs et les webps sont autorisé" 
            ])
            // ->add('slug')
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                'query_builder' => $this->category->getSubCategory(),
                'group_by' => 'parent.name',
                'placeholder' => 'Selectionner une categorie',
                'constraints' => [
                    new NotBlank([
                        'message' => 'La categorie doit être renseigné' 
                        ]),
                    ]
                ])
            // ->add('user', EntityType::class, ['class' => User::class])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Gif::class,
        ]);
    }
}
