<?php

namespace App\Twig;

use App\Repository\CategoryRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtentionExtension extends AbstractExtension
{
    // public function getFilters(): array
    // {
    //     return [
    //         // If your filter generates SAFE HTML, you should add a third
    //         // parameter: ['is_safe' => ['html']]
    //         // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
    //         new TwigFilter('filter_name', [$this, 'doSomething']),
    //     ];
    // }

    private CategoryRepository $categoryRepository;
    
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /*
        getFunctions: créer une fonction personnalisée dans twig
    */
    public function getFunctions(): array
    {
        /*
            parametres:
                nom de la fonction dans twig
                nom de la méthode php reliée à la fonction
        */
        return [
            new TwigFunction('get_categories', [$this, 'getCategories']),
        ];
    }

    public function getCategories():array
    {
        /*
            doctrine: 2 branches
                repository : essenciellement faire des selects
                EntityManager : UPDATE INSERT et DELETE

            méthodes de selection des repository
                find : recupération d'une entité avec l'id
                findAll : array d'entité
                findBy : array d'entité avec conditions
                find : recupération d'une entité avec condition
        */

        return $this->categoryRepository->findAll();
    }
}
